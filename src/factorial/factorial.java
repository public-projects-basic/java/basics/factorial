package factorial;

import java.util.Scanner;

/*
 * @author Erik Sergio Guzman Contreras
 * @created April/25/2019
 */
public class factorial {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		/* Variable para leer los datos del teclado */
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingresa número del cuál se optendra el factorial: ");
		/* Variable para almacenar el número obtenido del teclado */
		int numero = scan.nextInt();
		/* Variable para respuesta */
		int var = numero;
		/* Variable para multiplicar el número y obtener */
		long factorial = 1;
		/* Metdodo para recorrer el nùmero */
		while( numero != 0) {
			/* Almacenar el factorial */
			factorial=factorial*numero;
			/* Restar 1 al número para multiplicar */
			numero--;
						  
		}
		/* Imprimir el resultado en pantalla */
		System.out.println("El factorial de " + var + " es: " + factorial);
	
	}

}
